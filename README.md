# k8s-demo

A simple demo to demonstrate some features of k8s :
* namespace creation
* create a deployment
* create a service exposed externaly
* scale the application
* rolling upgrade of the app from version 1 to version 2

at the end run :

kubectl delete svc/hello-server

kubectl delete deploy/hello-server

kubectl delete namespace demo